package com.lineate.traineeship;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class EntityServiceTest {
    public static ServiceFactory serviceFactory = new ServiceFactory();
    public static UserService userService = serviceFactory.createUserService();
  //  public static EntityService entityService = serviceFactory.createEntityService();

    @Test
    public void testCreateEntityWithNullName(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        Assertions.assertFalse(serviceFactory.createEntityService().createEntity(user, null, "Value"));
    }

    @Test
    public void testCreateEntityWithEmptyName(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        Assertions.assertFalse(serviceFactory.createEntityService().createEntity(user, "", "Value"));
    }

    @Test
    public void testCreateEntityWithNameWithSpaces(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        Assertions.assertFalse(serviceFactory.createEntityService().createEntity(user, "Name with spaces", "Value"));
        Assertions.assertFalse(serviceFactory.createEntityService().createEntity(user, "    Tab", "Value"));
        Assertions.assertFalse(serviceFactory.createEntityService().createEntity(user, "\nNextLine", "Value"));
        Assertions.assertFalse(serviceFactory.createEntityService().createEntity(user, "\fPageRun", "Value"));
    }

    @Test
    public void testCreateEntityWithLargeName(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        Assertions.assertFalse(serviceFactory.createEntityService().createEntity(user, "VeryLargeNameVeryLargeNameVeryLar", "Value"));
    }

    @Test
    public void testCreateEntityWithNameLengthEqualTo32(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        Assertions.assertTrue(serviceFactory.createEntityService().createEntity(user, "VeryLargeNameVeryLargeNameVeryLa", "Value"));
    }

    @Test
    public void testGetEntityValue(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        EntityService entityService = serviceFactory.createEntityService();
        entityService.createEntity(user, "entity", "Value");
        Assertions.assertEquals("Value", entityService.getEntityValue(user, "entity"));
    }

    @Test
    public void testGetNonexistentEntityValue(){
        Collection<Permission> permissions = new ArrayList<Permission>(){{add(Permission.write);}};
        Group group = userService.createGroup("Name", permissions);
        User user = userService.createUser("Vasya", group);
        Assertions.assertNull(serviceFactory.createEntityService().getEntityValue(user, "entity"));
    }

    @Test
    public void testGetEntityValueWithoutPermission(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        EntityService entityService = serviceFactory.createEntityService();
        entityService.createEntity(user, "entity", "Value");
        User anotherUser = userService.createUser("Petya", group);
        Assertions.assertNull(entityService.getEntityValue(anotherUser, "entity"));
    }

    @Test
    public void testGetEntityValueByGroupMemberWithPermission(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.read));
        User user = userService.createUser("Vasya", group);
        EntityService entityService = serviceFactory.createEntityService();
        entityService.createEntity(user, "entity", "Value");
        User anotherUser = userService.createUser("Petya", group);
        Assertions.assertEquals("Value", entityService.getEntityValue(anotherUser, "entity"));
    }

    @Test
    public void testUpdateEntity(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        EntityService entityService = serviceFactory.createEntityService();
        entityService.createEntity(user, "entity", "Value");
        Assertions.assertTrue(entityService.updateEntity(user, "entity", "NewValue"));
        Assertions.assertEquals("NewValue", entityService.getEntityValue(user, "entity"));
    }

    @Test
    public void testUpdateNonexistentEntity(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        Assertions.assertFalse(serviceFactory.createEntityService().updateEntity(user, "entity", "NewValue"));
    }

    @Test
    public void testUpdateEntityWithoutPermission(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.read));
        User user = userService.createUser("Vasya", group);
        EntityService entityService = serviceFactory.createEntityService();
        entityService.createEntity(user, "entity", "Value");
        User anotherUser = userService.createUser("Petya", group);
        Assertions.assertFalse(entityService.updateEntity(anotherUser, "entity", "NewValue"));
        Assertions.assertEquals("Value", entityService.getEntityValue(anotherUser, "entity"));
    }

    @Test
    public void testUpdateEntityByGroupMemberWithPermission(){
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        EntityService entityService = serviceFactory.createEntityService();
        entityService.createEntity(user, "entity", "Value");
        User anotherUser = userService.createUser("Petya", group);
        Assertions.assertTrue(entityService.updateEntity(anotherUser, "entity", "NewValue"));
        Assertions.assertEquals("NewValue", entityService.getEntityValue(user, "entity"));
    }

    @Test
    public void testEntityGroupsImmutability(){
        Group group1 = userService.createGroup("group1", Collections.singleton(Permission.write));
        Group group2 = userService.createGroup("group2", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group1);
        EntityService entityService = serviceFactory.createEntityService();
        entityService.createEntity(user, "entity", "Value");
        Entity entity = entityService.getEntity("entity");
        Assertions.assertEquals(user.getGroups(), entity.getGroups());
        group2.addUser(user);
        Assertions.assertNotEquals(user.getGroups(), entity.getGroups());
    }
}