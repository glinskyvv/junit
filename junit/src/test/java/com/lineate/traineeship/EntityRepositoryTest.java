package com.lineate.traineeship;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;

public class EntityRepositoryTest {
    public static ServiceFactory serviceFactory = new ServiceFactory();

    @Test
    public void testRepositorySaveEntityOnCreation(){
        EntityRepository repository = Mockito.mock(EntityRepository.class);
        EntityService entityService = serviceFactory.createEntityService(repository);
        UserService userService = serviceFactory.createUserService();
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        Entity entity = new Entity(user, "entity");
        entity.setValue("Value");

        entityService.createEntity(user, "entity", "Value");

        Mockito.verify(repository).save(entity);
    }


}
