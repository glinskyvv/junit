package com.lineate.traineeship;

import org.junit.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;


public class UserServiceTest {
    public static ServiceFactory serviceFactory = new ServiceFactory();

    @Test
    public void testCreateGroup(){
        UserService userService = serviceFactory.createUserService();
        Collection<Permission> permissions = Collections.singleton(Permission.write);
        Group group = userService.createGroup("Name", permissions);
        Assertions.assertEquals("Name", group.getName());
        Assertions.assertEquals(permissions, group.getPermissions());
    }

    @Test
    public void testCreateUser(){
        UserService userService = serviceFactory.createUserService();
        Group group = userService.createGroup("Name", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group);
        Assertions.assertEquals("Vasya", user.getName());
        Assertions.assertEquals(Collections.singleton(group), user.getGroups());
    }

    @Test
    public void testAddUserToGroup(){
        UserService userService = serviceFactory.createUserService();
        Group group1 = userService.createGroup("group1", Collections.singleton(Permission.write));
        Group group2 = userService.createGroup("group2", Collections.singleton(Permission.write));
        User user = userService.createUser("Vasya", group1);
        group2.addUser(user);
        Assertions.assertEquals(2, user.getGroups().size());
    }
}
